package sk.azet.realmapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import sk.azet.realmapp.model.Issue;
import sk.azet.realmapp.model.User;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private Realm realm;

    private RealmChangeListener<RealmResults<Issue>> issueRealmChangeListener = new RealmChangeListener<RealmResults<Issue>>() {
        @Override
        public void onChange(RealmResults<Issue> issues) {
            Log.d(TAG, String.format("Issues count = %d", issues.size()));
        }
    };

    private RealmResults<Issue> issues;
    private RecyclerView issuesList;
    private RecyclerView.LayoutManager issuesListLayoutManager;
    private IssuesAdapter issuesAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        issuesList = (RecyclerView) findViewById(R.id.issues_list);
        issuesListLayoutManager = new LinearLayoutManager(this);

        issuesList.setHasFixedSize(true);
        issuesList.setLayoutManager(issuesListLayoutManager);


        Realm.init(this);
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());
        realm = Realm.getDefaultInstance();

        issues = realm.where(Issue.class).findAllAsync();
        issues.addChangeListener(issueRealmChangeListener);

        issuesAdapter = new IssuesAdapter(this, issues, true);
        issuesList.setAdapter(issuesAdapter);

    }

    @Override
    protected void onStop() {
        issues.removeChangeListener(issueRealmChangeListener);
        // issues.removeChangeListeners();
        super.onStop();
    }

    public void onAddIssueBtnClicked(View view) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                User author = realm.where(User.class).equalTo("id", 1).findFirst();
                Issue issue = new Issue(System.currentTimeMillis(), "New issue", author);
                realm.copyToRealm(issue);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, "Realm Error", error);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    class IssuesAdapter extends RealmRecyclerViewAdapter<Issue, IssuesAdapter.IssueViewHolder> {

        public IssuesAdapter(@NonNull Context context, @Nullable OrderedRealmCollection<Issue> data, boolean autoUpdate) {
            super(context, data, autoUpdate);
        }

        @Override
        public IssueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View issueView = LayoutInflater.from(MainActivity.this).inflate(R.layout.issue_row, parent, false);
            return new IssueViewHolder(issueView);
        }

        @Override
        public void onBindViewHolder(IssueViewHolder holder, int position) {
            holder.bind(getData().get(position));
        }

        class IssueViewHolder extends RecyclerView.ViewHolder {

            TextView issueTitle;

            public IssueViewHolder(View itemView) {
                super(itemView);
                issueTitle = (TextView) itemView.findViewById(R.id.issue_title);
            }

            public void bind(Issue issue) {
                issueTitle.setText(issue.getDescription());
            }
        }
    }
}
