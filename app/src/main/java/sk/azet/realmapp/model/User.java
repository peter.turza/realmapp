package sk.azet.realmapp.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by turza on 30/11/2016.
 */

public class User extends RealmObject {
    @PrimaryKey
    private long id;
    private String username;

    public User() {
    }

    public User(long id, String username) {
        this.id = id;
        this.username = username;
    }
}
