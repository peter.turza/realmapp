package sk.azet.realmapp.model;


import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by turza on 30/11/2016.
 */

public class Issue extends RealmObject {
    @PrimaryKey
    private long id;
    private String title;
    private User author;
    private RealmList<User> watchers;

    public Issue() {
    }

    public Issue(long id, String title, User author) {
        this.id = id;
        this.title = title;
        this.author = author;
    }

    public RealmList<User> getWatchers() {
        return watchers;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return String.format("%d: %s", id, getTitle());
    }
}
